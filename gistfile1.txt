Download a youtube sound track from a video and extract a subset (for example for a phone ringtone)
```
$ youtube-dl --extract-audio --audio-format mp3  https://youtu.be/lhlvV7mjeJM
$ ffmpeg -ss 00:00:45 -t 00:00:45 -i La\ vita\ è\ bella\ -\ Colonna\ sonora\ \(original\ soundtrack\)\ -\ brano\ -\ \'La\ vita\ è\ bella\'-lhlvV7mjeJM.mp3 lveb.mp3
```